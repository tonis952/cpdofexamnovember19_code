package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest{
@Test
    public void CompareCars1() throws Exception{
    int results = new MinMax().ford(0,0);
    assertEquals("Test Cars", 0, results);
}
@Test
    public void CompareCars2() throws Exception{
    int results = new MinMax().ford(1,1);
    assertEquals("Test Cars", 1, results);
}
}
